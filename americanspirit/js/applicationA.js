function validate() {
	if (isWhitespace (document.MemberForm.membername.value)) {
		alert("Please enter your first name or nickname.");
		document.MemberForm.membername.focus();
		document.MemberForm.membername.select();
		return false;
	}
	if (isWhitespace (document.MemberForm.headline.value)) {
		alert("Please enter your headline.");
		document.MemberForm.headline.focus();
		document.MemberForm.headline.select();
		return false;
	}
	if (document.MemberForm.gender.selectedIndex == 0 ) { 
		alert("Please choose from My Gender options");
		document.MemberForm.gender.focus();
		return false;
	}	 
	if (document.MemberForm.seeking.selectedIndex == 0 ) { 
		alert("Please choose from I am Seeking options");
		document.MemberForm.seeking.focus();
		return false;
	}	 
	if (document.MemberForm.purpose.selectedIndex == 0 ) { 
		alert("Please choose from For options");
		document.MemberForm.purpose.focus();
		return false;
	}	 
	if (document.MemberForm.desireageFrom.selectedIndex == 0 ) { 
		alert("Please choose from Desired Age Range From options");
		document.MemberForm.desireageFrom.focus();
		return false;
	}	 
	if (document.MemberForm.desireageTo.selectedIndex == 0 ) { 
		alert("Please choose from Desired Age Range To options");
		document.MemberForm.desireageTo.focus();
		return false;
	}	 	
	if (document.MemberForm.desireageFrom.selectedIndex > document.MemberForm.desireageTo.selectedIndex ) { 
		alert("Please choose Valid Age Range");
		document.MemberForm.desireageTo.focus();
		return false;
	}	 	
		if (document.MemberForm.marstatus.selectedIndex == 0 ) { 
		alert("Please choose from Current Relationship Status options");
		document.MemberForm.marstatus.focus();
		return false;
	}	 
	if (document.MemberForm.bodytype.selectedIndex == 0 ) { 
		alert("Please choose from My Body Type options");
		document.MemberForm.bodytype.focus();
		return false;
	}	 
	if (document.MemberForm.height.selectedIndex == 0 ) { 
		alert("Please choose from My Height options");
		document.MemberForm.height.focus();
		return false;
	}	 
	if (document.MemberForm.haircolor.selectedIndex == 0 ) { 
		alert("Please choose from My Hair Color options");
		document.MemberForm.haircolor.focus();
		return false;
	}	 
	if (document.MemberForm.eyecolor.selectedIndex == 0 ) { 
		alert("Please choose from My Eye Color options");
		document.MemberForm.eyecolor.focus();
		return false;
	}	 
	if (document.MemberForm.ethnicgroup.selectedIndex == 0 ) { 
		alert("Please choose from My Ethnic Group options");
		document.MemberForm.ethnicgroup.focus();
		return false;
	}	 

	if (document.MemberForm.education.selectedIndex == 0 ) { 
		alert("Please choose from My Education options");
		document.MemberForm.education.focus();
		return false;
	}	 
	if (document.MemberForm.sign.selectedIndex == 0 ) { 
		alert("Please choose from My Astrological Sign options");
		document.MemberForm.sign.focus();
		return false;
	}	 
	if (document.MemberForm.pets.selectedIndex == 0 ) { 
		alert("Please choose from Do I Have Pets options");
		document.MemberForm.pets.focus();
		return false;
	}	 
	if (document.MemberForm.havechildren.selectedIndex == 0 ) { 
		alert("Please choose from Do I Have Children options");
		document.MemberForm.havechildren.focus();
		return false;
	}	 
	if (document.MemberForm.wantchildren.selectedIndex == 0 ) { 
		alert("Please choose from Do I Want Children options");
		document.MemberForm.wantchildren.focus();
		return false;
	}	 
	if (document.MemberForm.relocate.selectedIndex == 0 ) { 
		alert("Please choose from Would I Relocate ... options");
		document.MemberForm.relocate.focus();
		return false;
	}	 
	if (isWhitespace (document.MemberForm.city.value)) {
		alert("Please enter your city.");
		document.MemberForm.city.focus();
		document.MemberForm.city.select();
		return false;
	}

	if (document.MemberForm.state.selectedIndex == 0 ) { 
		alert("Please choose from State or Province options");
		document.MemberForm.state.focus();
		return false;
	}	 
	if ((document.MemberForm.country.selectedIndex == 1  )
		&& (document.MemberForm.state.selectedIndex < 2 
		|| document.MemberForm.state.selectedIndex > 52) ) { 
		alert("Please choose valid US State");
		document.MemberForm.state.focus();
		return false;
	}	 
	if ((document.MemberForm.country.selectedIndex == 2  )
		&& document.MemberForm.state.selectedIndex < 53 ) { 
		alert("Please choose valid Canadian Province");
		document.MemberForm.state.focus();
		return false;
	}	 
	if ((document.MemberForm.country.selectedIndex > 2  )
		&& document.MemberForm.state.selectedIndex != 1 ) { 
		alert("If Country is not US or Canada, State must be Not Applicable");
		document.MemberForm.state.options[1].selected = true;
		document.MemberForm.state.focus();
		return false;
	}	 
	if (document.MemberForm.country.selectedIndex == 0 ) { 
		alert("Please choose from Country options");
		document.MemberForm.country.focus();
		return false;
	}	 
	if ( (document.MemberForm.country.selectedIndex == 1 
		|| document.MemberForm.country.selectedIndex == 2  )
		&& (isWhitespace (document.MemberForm.areacode.value) 
		|| !isaNum (document.MemberForm.areacode.value))) {
		alert("Numeric Area Code must be entered for United States and Canada.");
		document.MemberForm.areacode.focus();
		document.MemberForm.areacode.select();
		return false;
	}
	if ( (document.MemberForm.country.selectedIndex == 1 
		|| document.MemberForm.country.selectedIndex == 2  )
		&& isaNum (document.MemberForm.areacode.value)
		&& (document.MemberForm.areacode.value < 100  
		|| document.MemberForm.areacode.value > 999)) {
		alert("3-digit Area Code must be entered for United States and Canada.");
		document.MemberForm.areacode.focus();
		document.MemberForm.areacode.select();
		return false;
	}
	if ( !(document.MemberForm.country.selectedIndex == 1 
		|| document.MemberForm.country.selectedIndex == 2 )
		&& (!isWhitespace (document.MemberForm.areacode.value))) {
		alert("Area Code must be left blank if not in United States or Canada.");
		document.MemberForm.areacode.focus();
		document.MemberForm.areacode.select();
		return false;
	}
	if (document.MemberForm.country.selectedIndex == 1 
		&& (isWhitespace (document.MemberForm.searchzip.value) 
		|| !isaNum (document.MemberForm.searchzip.value))) {
		alert("Numeric Zip Code must be entered for United States.");
		document.MemberForm.searchzip.focus();
		document.MemberForm.searchzip.select();
		return false;
	}
	if (document.MemberForm.country.selectedIndex == 1 
		&& isaNum (document.MemberForm.searchzip.value)
		&& (document.MemberForm.searchzip.value.length != 5)) {
		alert("5-digit Zip Code must be entered for United States.");
		document.MemberForm.searchzip.focus();
		document.MemberForm.searchzip.select();
		return false;
	}
	if ( document.MemberForm.country.selectedIndex != 1 
		&& (!isWhitespace (document.MemberForm.searchzip.value))) {
		alert("Zip Code must be left blank if not in United States.");
		document.MemberForm.searchzip.focus();
		document.MemberForm.searchzip.select();
		return false;
	}
	if (document.MemberForm.smoking.selectedIndex == 0 ) { 
		alert("Please choose from My Smoking Preference options");
		document.MemberForm.smoking.focus();
		return false;
	}	 
	if (document.MemberForm.drinking.selectedIndex == 0 ) { 
		alert("Please choose from My Drinking Preference options");
		document.MemberForm.drinking.focus();
		return false;
	}	 
	if (isWhitespace (document.MemberForm.occupation.value)) {
		alert("Please enter your occupation.");
		document.MemberForm.occupation.focus();
		return false;
	}
	if (document.MemberForm.occupation.value.length > 255) {
		alert("Occupation must not be greater than 255 characters.");
		document.MemberForm.occupation.focus();
		return false;
	}
	return true;	
}
