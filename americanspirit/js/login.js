function validateLogin() {
	if (isWhitespace (document.LoginForm.memberID.value)) {
		alert("Please enter your Member ID.");
		document.LoginForm.memberID.focus();
		document.LoginForm.memberID.select();
		return false;
	}
	if (!isaNum(document.LoginForm.memberID.value)) {
		alert("MemberID must be numeric.")
		document.LoginForm.memberID.focus();
		document.LoginForm.memberID.select();
		return false;
	}
	if (isWhitespace (document.LoginForm.pw.value)) {
		alert("Please enter your Password.");
		document.LoginForm.pw.focus();
		document.LoginForm.pw.select();
		return false;
	}
	return true;	
}

function validateEmailPwd() {
	if (isWhitespace (document.EmailPwdForm.email.value) && isWhitespace(document.EmailPwdForm.emailmemberID.value)) {
		alert("Please enter Email Address or MemberID.");
		document.EmailPwdForm.email.focus();
		document.EmailPwdForm.email.select();
		return false;
	}
	return true;	
}

 
