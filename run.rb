require 'mechanize'
@agent = Mechanize.new
#Криво создает папки
url = 'https://web.archive.org/web/20080624175602/http://www.online-dating-matchmaking.com:80/'

#Забираем все линки со страницы
def get_links url
    all_links = []
    page = @agent.get url
    page.links_with.each do |link|
        if link.href.include?('www.online-dating-matchmaking')
            all_links << link.href
        end
        if !link.href.include?('.com') && !link.href.include?('web.archive.org') && !link.href.include?('.org') && !link.href.include?('http')
            all_links << "#{url}#{link.href}"
        end
    end   
    return all_links
end

def get_images url
    page = @agent.get url
    images = page.search("img")
    arr_images =[]
    images.each do |image|
        url = "https://web.archive.org#{image.attributes["src"].value}"
       
        if !arr_images.include?(url)
            arr_images << url
        end
    end
    
    arr_images.each {|l|
        begin
            name = l.split('/')[-1]
            #СДЕЛАТЬ ПРОВЕРКУ НАЛИЧИЯ ФАЙЛОВ
            @agent.get(l).save "img/#{name}"
        rescue => exception
            p exception
        end    
    }
end

def get_images_from_all_site url
    urls = get_links url
    urls.each { |link| 

        get_images link
    }
end

def wget_pages url
    get_links(url).each_with_index { |link, index| 
    name = link.split(':80/')[1]
    if name == nil
        @agent.get(link).save "index-#{index}.html"
    elsif name.include?('/')
        file_name = name.split('/')[-1]
        dirs = name.split('/')
        dirs.pop
        dirs.each do |d|
            Dir.mkdir d
            Dir.chdir d
        end
        @agent.get(link).save file_name
        Dir.getwd
    else
        @agent.get(link).save name
    end
    }
end

def get_css url
    page = @agent.get url
    p page.css

end
